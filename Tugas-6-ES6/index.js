//jawaban Soal 1
    // const persegi = (p,l) => {
    //     const luas = p * l
    //     const keliling =  2 * (p+l)
    //     return ("Luas " + luas + " Keliling " + keliling)
    // }
    const panjang = 3
    const lebar = 2
    
    let keliling = (p,l) => 2 * (p + l)
    let luas = (p,l) => p * l

    console.log("Jawban Soal 1")
    console.log("Keliling = " + keliling(panjang,lebar))
    console.log("Liuas = " + luas(panjang,lebar))
    console.log()

//Jawaban Soal 2
    const  name = {
        fullname : (first,last) => first + " " + last
    }
    console.log("Jawaban Soal 2")
    console.log(name.fullname("William","Imoh"))
    console.log()

//Jawaban Soal 3
    const Bio = {
        firstName: "Muhammad",
        lastName: "Iqbal Mubarok",
        address: "Jalan Ranamanyar",
        hobby: "playing football",
    }

    const {firstName,lastName,address,hobby} = Bio
    
    console.log("Jawaban Soal 3")
    console.log("Nama " + firstName + " " + lastName + ", Alamat " + address + ", Hobby " + hobby)
    console.log()

//Jawaban Soal 4
    const west = ["Will", "Chris", "Sam", "Holly"]
    const east = ["Gill", "Brian", "Noel", "Maggie"]
    let combined = [...west ,...east]
    
    console.log("Jawaban Soal 4")
    console.log(combined)
    console.log()

//Jawaban Soal 5
    const planet = "earth" 
    const view = "glass" 
    const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

    console.log("Jawaban Soal 5")
    console.log(before)